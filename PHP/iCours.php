<?php
interface iCours{
           const MAXETUDIANTS = 30;
           
           public function getTitre(); 
           
           public function getSigle();
           
           public function getLocal($groupe); 
           
}

