<?php
require 'iCours.php';

class Cours2 implements iCours {
    
           private $sigle = "";
           private $titre = "";
           private $locaux = array();
           
           public function getTitre() {
               return $this->titre;
           }
           
           public function getSigle() {
               return $this->sigle;
           }
           
           public function getLocal($groupe) {
               $local = "Aucun";
               if(!empty($this->locaux[$groupe])){
                   $local = $this->locaux[$groupe];
               }
               return $local;
           }
           
           public function setTitre($newTitre) {
               $this->titre = $newTitre;
           }
           
           public function addGroupe($groupe,$local) {
               if(!empty($groupe) and !empty($local)){
                   $this->locaux[$groupe]=$local;
               }
               
           }
           
           function __construct($pSigle,$pTitre){
               //constructeur
               $this->sigle = $pSigle;
               $this->titre = $pTitre;
           }
           
}

//Exemple utilisation classe
$c = new Cours2('INF2005','Programmation Web');
$c->addGroupe(40, "SH-2420");
echo  $c->getSigle() . "\n";
echo  $c->getTitre(). "\n";
echo  $c->getLocal(10). "\n";
echo  $c->getLocal(40). "\n";
// tester en ligne de commande
// php Cours2.php
