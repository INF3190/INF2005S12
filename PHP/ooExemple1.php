<?php
class Cours{
           const MAXETUDIANTS = 30;
           private $sigle = "";
           private $titre = "";
           
           public function getTitre() {
               return $this->titre;
           }
           
           public function getSigle() {
               return $this->sigle;
           }
           
           protected function setSigle($newSigle) {
               $this->sigle = $newSigle;
           }
           
           protected function setTitre($newTitre) {
               $this->titre = $newTitre;
           }
           
           function __construct($pSigle,$pTitre){
               //constructeur
               $this->sigle = $pSigle;
               $this->titre = $pTitre;
           }
           
}

//Exemple utilisation classe
$c = new Cours('INF2005','Programmation Web');
echo  $c->getSigle() . "\n";
echo  $c->getTitre(). "\n";
// tester en ligne de commande
// php ooExemple1.php
